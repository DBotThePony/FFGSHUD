
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local FFGSHUD = FFGSHUD
local language = language
local Color = Color
local RealTimeL = RealTimeL
local table = table
local math = math
local Vector = Vector

FFGSHUD.ENABLE_KILLFEED = FFGSHUD:CreateConVar('killfeed', '1', 'Enable HUD killfeed')

local notices = {}
local function npcColor(npc)
	local crc = tonumber(util.CRC(npc))
	local r = crc % 255
	crc = crc - r
	local g = (crc / 255) % 255
	crc = crc / 255 - g
	local b = (crc / 255) % 255
	return Color(r:abs(), g:abs(), b:abs())
end

function FFGSHUD:AddDeathNotice(attacker, attackerTeam, inflictor, victim, victimTeam)
	if not self.ENABLE_KILLFEED:GetBool() then return end
	self:AddDeathNotice2(attacker, attackerTeam, inflictor, victim, victimTeam)
	return true
end

function FFGSHUD:AddDeathNotice2(attacker, attackerTeam, inflictor, victim, victimTeam)
	local validWeapon = inflictor ~= nil and inflictor ~= 'suicide'
	local validAttacker = attacker ~= nil and attacker ~= victim
	local worldspawn = attacker == '#world'

	local isSuicide = not validWeapon and not validAttacker or attacker == victim
	local weapon = worldspawn and 'fall damage'
		or isSuicide and language.GetPhrase('suicide') or
		(inflictor and (inflictor ~= 'worldspawn' and language.GetPhrase(inflictor) or language.GetPhrase(attacker))) or
		'???'

	weapon = weapon:upper()

	local gravity = math.random() < 0.1
	local entry = {}

	entry.weaponColor = Color(255, 255, 255)
	entry.isFallDamage = worldspawn
	entry.isSuicide = isSuicide
	entry.weapon = '[' .. weapon .. ']'
	entry.validAttacker = validAttacker

	local displayTime = ((not isSuicide and (attacker and #attacker / 4 or 0) or 0) + (#weapon / 4) + (#victim / 2)):clamp(4, 8)
	local animtime = displayTime * 0.1

	entry.ttl = RealTimeL() + displayTime
	entry.fadeInStart = RealTimeL()
	entry.fadeInEnd = RealTimeL() + animtime

	entry.fadeOutStart = RealTimeL() + displayTime - animtime
	entry.fadeOutEnd = RealTimeL() + displayTime

	entry.victim = victim
	entry.attacker = attacker

	if validAttacker then
		if attackerTeam and attackerTeam > 0 and attackerTeam ~= TEAM_UNASSIGNED then
			entry.attackerColor = team.GetColor(attackerTeam)
		else
			entry.attackerColor = npcColor(attacker)
		end
	else
		entry.attackerColor = Color(67, 158, 184)
	end

	if victimTeam and victimTeam > 0 and victimTeam ~= TEAM_UNASSIGNED then
		entry.victimColor = team.GetColor(victimTeam)
	else
		entry.victimColor = npcColor(victim)
	end

	entry.gravity = gravity

	if gravity then
		entry.velocityAttacker = {math.random(-100, 25) / 25, math.random(-15, 5)}
		entry.velocityWeapon = {math.random(-25, 25) / 25, math.random(-25, 5)}
		entry.velocityVictim = {math.random(-25, 100) / 25, math.random(-17, 5)}
		entry.posAttacker = {0, 0}
		entry.posWeapon = {0, 0}
		entry.posVictim = {0, 0}

		entry.ttl = RealTimeL() + displayTime * 2
	end

	table.insert(notices, entry)
	return entry
end

function FFGSHUD:HUDCommons_EnterEditMode_Killfeed()
	local entry = self:AddDeathNotice2('Attacker', -1, 'weapon', 'Victim', -1)
	entry.test = true
	-- ook
	local displayTime = 3600
	local animtime = 1

	entry.ttl = RealTimeL() + displayTime
	entry.fadeInStart = RealTimeL()
	entry.fadeInEnd = RealTimeL() + animtime

	entry.fadeOutStart = RealTimeL() + displayTime - animtime
	entry.fadeOutEnd = RealTimeL() + displayTime

	entry = self:AddDeathNotice2('worldspawn', -1, 'worldspawn', 'Victim', -1)
	entry.test = true

	entry.ttl = RealTimeL() + displayTime
	entry.fadeInStart = RealTimeL()
	entry.fadeInEnd = RealTimeL() + animtime

	entry.fadeOutStart = RealTimeL() + displayTime - animtime
	entry.fadeOutEnd = RealTimeL() + displayTime

	entry = self:AddDeathNotice2('Victim', -1, 'suicide', 'Victim', -1)
	entry.test = true

	entry.ttl = RealTimeL() + displayTime
	entry.fadeInStart = RealTimeL()
	entry.fadeInEnd = RealTimeL() + animtime

	entry.fadeOutStart = RealTimeL() + displayTime - animtime
	entry.fadeOutEnd = RealTimeL() + displayTime
end

function FFGSHUD:HUDCommons_ExitEditMode_Killfeed()
	local rem = {}

	for i, entry in ipairs(notices) do
		if entry.test then
			table.insert(rem, i)
		end
	end

	table.removeValues(notices, rem)
end

local DRAW_POS, DRAWSIDE = FFGSHUD:DefinePosition('killfeed', 0.85, 0.12)
local surface = surface
local ScreenSize = ScreenSize
local render = render

function FFGSHUD:DrawDeathNoticeRight()
	local x, y = DRAW_POS()
	x = x - self.BATTLE_STATS_WIDE
	local space = ScreenSize(3)
	local time = RealTimeL()
	local H = self.KillfeedFont.REGULAR_SIZE_H
	local HScr = ScrHL()

	for i, entry in ipairs(notices) do
		local x = x
		local mult = 1
		local cut = false
		local gravity = entry.gravity

		if entry.fadeInEnd > time then
			cut = true
			mult = time:progression(entry.fadeInStart, entry.fadeInEnd)
			render.PushScissorRect(0, y, x, y + H * mult)
		elseif entry.fadeOutStart < time then
			mult = 1 - time:progression(entry.fadeOutStart, entry.fadeOutEnd)

			if not gravity then
				cut = true
				render.PushScissorRect(0, y, x, y + H * mult)
			end
		end

		local addx, addy = gravity and entry.posVictim[1] or 0, gravity and entry.posVictim[2] or 0
		local w, h = self:DrawShadowedTextAligned(self.KillfeedFont, entry.victim, x + addx, y + addy, entry.victimColor)
		x = x - w - ScreenSize(5)

		local addx, addy = gravity and entry.posWeapon[1] or 0, gravity and entry.posWeapon[2] or 0
		w, h = self:DrawShadowedTextAligned(self.KillfeedFont, entry.weapon, x + addx, y + addy, entry.weaponColor)

		if entry.validAttacker then
			x = x - w - ScreenSize(5)
			local addx, addy = gravity and entry.posAttacker[1] or 0, gravity and entry.posAttacker[2] or 0
			self:DrawShadowedTextAligned(self.KillfeedFont, entry.attacker, x + addx, y + addy, entry.attackerColor)
		end

		y = y + (h + space) * mult

		if cut then
			render.PopScissorRect()
		end

		if y > HScr then return end
	end
end

local ScrWL = ScrWL
local ScrHL = ScrHL

function FFGSHUD:DrawDeathNoticeLeft()
	local x, y = DRAW_POS()
	local space = ScreenSize(3)
	local time = RealTimeL()
	local H = self.KillfeedFont.REGULAR_SIZE_H
	local W = ScrWL()
	local H = ScrHL()

	for i, entry in ipairs(notices) do
		local x = x
		local mult = 1
		local cut = false
		local gravity = entry.gravity

		if entry.fadeInEnd > time then
			cut = true
			mult = time:progression(entry.fadeInStart, entry.fadeInEnd)
			render.PushScissorRect(0, y, W, y + H * mult)
		elseif entry.fadeOutStart < time then
			mult = 1 - time:progression(entry.fadeOutStart, entry.fadeOutEnd)

			if not gravity then
				cut = true
				render.PushScissorRect(0, y, W, y + H * mult)
			end
		end

		if entry.validAttacker then
			local addx, addy = gravity and entry.posVictim[1] or 0, gravity and entry.posVictim[2] or 0
			local w, h = self:DrawShadowedText(self.KillfeedFont, entry.attacker, x + addx, y + addy, entry.victimColor)
			x = x + w + ScreenSize(5)
		end

		local addx, addy = gravity and entry.posWeapon[1] or 0, gravity and entry.posWeapon[2] or 0
		local w, h = self:DrawShadowedText(self.KillfeedFont, entry.weapon, x + addx, y + addy, entry.weaponColor)

		x = x + w + ScreenSize(5)
		local addx, addy = gravity and entry.posAttacker[1] or 0, gravity and entry.posAttacker[2] or 0
		self:DrawShadowedText(self.KillfeedFont, entry.victim, x + addx, y + addy, entry.attackerColor)

		y = y + (h + space) * mult

		if cut then
			render.PopScissorRect()
		end

		if y > H then return end
	end
end

function FFGSHUD:DrawDeathNoticeCenter()
	local x, y = DRAW_POS()
	local space = ScreenSize(3)
	local time = RealTimeL()
	local H = self.KillfeedFont.REGULAR_SIZE_H
	local W = ScrWL()
	local HScr = ScrHL()

	for i, entry in ipairs(notices) do
		local x = x
		local mult = 1
		local cut = false
		local gravity = entry.gravity

		if entry.fadeInEnd > time then
			cut = true
			mult = time:progression(entry.fadeInStart, entry.fadeInEnd)
			render.PushScissorRect(0, y, W, y + H * mult)
		elseif entry.fadeOutStart < time then
			mult = 1 - time:progression(entry.fadeOutStart, entry.fadeOutEnd)

			if not gravity then
				cut = true
				render.PushScissorRect(0, y, W, y + H * mult)
			end
		end

		local addx, addy = gravity and entry.posVictim[1] or 0, gravity and entry.posVictim[2] or 0
		local w, h = self:DrawShadowedTextCentered(self.KillfeedFont, entry.weapon, x + addx, y + addy, entry.weaponColor)

		local addx, addy = gravity and entry.posWeapon[1] or 0, gravity and entry.posWeapon[2] or 0
		self:DrawShadowedText(self.KillfeedFont, entry.victim, x + addx+ ScreenSize(35), y + addy, entry.victimColor)

		if entry.validAttacker then
			local addx, addy = gravity and entry.posAttacker[1] or 0, gravity and entry.posAttacker[2] or 0
			self:DrawShadowedTextAligned(self.KillfeedFont, entry.attacker, x + addx - ScreenSize(35), y + addy, entry.attackerColor)
		end

		y = y + (h + space) * mult

		if cut then
			render.PopScissorRect()
		end

		if y > HScr then return end
	end
end

function FFGSHUD:DrawDeathNotice()
	if not self.ENABLE_KILLFEED:GetBool() then return end

	local side = DRAWSIDE()

	if side == 'LEFT' then
		self:DrawDeathNoticeLeft()
	elseif side == 'RIGHT' then
		self:DrawDeathNoticeRight()
	else
		self:DrawDeathNoticeCenter()
	end
end

function FFGSHUD:ThinkDeathNotice()
	local toRemove
	local time = RealTimeL()
	local ftime = RealFrameTime() * 11

	for i, entry in ipairs(notices) do
		if entry.ttl < time then
			toRemove = toRemove or {}
			table.insert(toRemove, i)
		elseif entry.gravity and entry.fadeOutStart < time then
			local velx, vely = entry.velocityAttacker[1], entry.velocityAttacker[2]
			velx = velx * 0.98
			vely = vely + ftime
			entry.posAttacker[1] = entry.posAttacker[1] + velx
			entry.posAttacker[2] = entry.posAttacker[2] + vely
			entry.velocityAttacker[1], entry.velocityAttacker[2] = velx, vely

			velx, vely = entry.velocityWeapon[1], entry.velocityWeapon[2]
			velx = velx * 0.98
			vely = vely + ftime
			entry.posWeapon[1] = entry.posWeapon[1] + velx
			entry.posWeapon[2] = entry.posWeapon[2] + vely
			entry.velocityWeapon[1], entry.velocityWeapon[2] = velx, vely

			velx, vely = entry.velocityVictim[1], entry.velocityVictim[2]
			velx = velx * 0.98
			vely = vely + ftime
			entry.posVictim[1] = entry.posVictim[1] + velx
			entry.posVictim[2] = entry.posVictim[2] + vely
			entry.velocityVictim[1], entry.velocityVictim[2] = velx, vely
		end
	end

	if toRemove then
		table.removeValues(notices, toRemove)
	end
end

FFGSHUD:AddHookCustom('AddDeathNotice', 'AddDeathNotice')
FFGSHUD:AddPaintHook('DrawDeathNotice')
FFGSHUD:AddThinkHook('ThinkDeathNotice')
FFGSHUD:AddHookCustom('HUDCommons_EnterEditMode', 'HUDCommons_EnterEditMode_Killfeed')
FFGSHUD:AddHookCustom('HUDCommons_ExitEditMode', 'HUDCommons_ExitEditMode_Killfeed')
