
-- Copyright (C) 2018 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local FFGSHUD = FFGSHUD

FFGSHUD.ENABLE_COMPASS = FFGSHUD:CreateConVar('compass', '1', 'Enable HUD compass')

local DRAWPOS = FFGSHUD:DefinePosition('compass', 0.5, 0.04, false)
local ipairs = ipairs
local HUDCommons = DLib.HUDCommons
local surface = surface
local ScreenSize = ScreenSize
local render = render
local color_white = FFGSHUD:CreateColorN('compass', 'Compass Color', Color())
local ColorDash = FFGSHUD:CreateColorN('compass_dash', 'Compass Dash Color', Color(170, 225, 150))
local ScrWL, ScrHL = ScrWL, ScrHL
local ScreenSize = ScreenSize
local TEXT_DISPERSION_SHIFT = 0.0175
local i18n = DLib.i18n

local directions = {
	'gui.ffgshud.compass.south',
	'gui.ffgshud.compass.south_east',
	'gui.ffgshud.compass.east',
	'gui.ffgshud.compass.north_east',
	'gui.ffgshud.compass.north',
	'gui.ffgshud.compass.north_west',
	'gui.ffgshud.compass.west',
	'gui.ffgshud.compass.south_west',
}

local TOP_COLOR = Color(255, 90, 133, 200)
local BOTTOM_COLOR = Color(35, 240, 240, 200)

local F_COLOR = Color(255, 0, 0)
local S_COLOR = Color(0, 255, 0)
local T_COLOR = Color(0, 0, 255)

local function drawMarkers(self, x, y, angle, shiftby)
	local color_white = color_white()
	TOP_COLOR.a = color_white.a
	BOTTOM_COLOR.a = color_white.a
	x, y = x:floor(), y:floor()
	local mult = ScreenSize(1.25)

	for i, dir in ipairs(directions) do
		local lang = ((i + shiftby - 1) * 45)

		self:DrawShadowedTextCentered(
			self.CompassDirections,
			i18n.localize(dir),
			(x - (lang - angle) * mult):floor(),
			y - (self.CompassDirections.REGULAR_ADDITIVE_SIZE_H * TEXT_DISPERSION_SHIFT):max(1):floor(),
			color_white
		)
	end

	y = y + ScreenSize(2)
	local wide, tall = ScreenSize(1):max(1):round(), ScreenSize(6):round()

	for i = 1, (#directions - 2) * 4 + 1 do
		local lang = ((i + shiftby - 1) * 15)

		if lang % 45 ~= 0 then
			if self.ENABLE_DISPERSION:GetBool() then
				surface.SetDrawColor(TOP_COLOR)
				surface.DrawRect((x - (lang - angle) * mult - wide / 2):floor(), y - (self.CompassDirections.REGULAR_ADDITIVE_SIZE_H * TEXT_DISPERSION_SHIFT):max(1):floor(), wide, tall)

				surface.SetDrawColor(BOTTOM_COLOR)
				surface.DrawRect((x - (lang - angle) * mult - wide / 2):floor(), y + (self.CompassDirections.REGULAR_ADDITIVE_SIZE_H * TEXT_DISPERSION_SHIFT):max(1):floor(), wide, tall)
			end

			surface.SetDrawColor(color_white)
			surface.DrawRect((x - (lang - angle) * mult - wide / 2):floor(), y, wide, tall)
		end
	end
end

function FFGSHUD:DrawCompass(ply)
	if not self:GetVarAlive() or not self.ENABLE_COMPASS:GetBool() then return end
	local yaw = ply:EyeAnglesFixed().y
	local angle = yaw
	local color_white = color_white()

	if angle < 0 then
		angle = 360 + angle
	end

	local x, y = DRAWPOS()

	if self.ENABLE_DISPERSION:GetBool() then
		surface.SetFont(self.CompassDirections.REGULAR_ADDITIVE)
	else
		surface.SetFont(self.CompassDirections.REGULAR)
	end

	surface.SetTextColor(color_white)

	render.PushScissorRect(x - ScreenSize(180), y, x + ScreenSize(180), y + ScreenSize(80))

	surface.SetDrawColor(ColorDash())
	local wide, tall = ScreenSize(2):max(1):round(), ScreenSize(8):round()

	surface.DrawRect(x - wide / 2, y, wide, tall)

	drawMarkers(self, x, y, angle, 0)

	if angle < 110 then
		drawMarkers(self, x, y, angle, -#directions)
	end

	if angle > 220 then
		drawMarkers(self, x, y, angle, #directions)
	end

	render.PopScissorRect()

	self:DrawShadowedTextCentered(self.CompassAngle, angle:floor(), x, y + ScreenSize(10), color_white)
end

FFGSHUD:AddPaintHook('DrawCompass')
